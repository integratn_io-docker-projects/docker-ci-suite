#!groovy
 
import jenkins.model.*
import hudson.security.*
import jenkins.security.s2m.AdminWhitelistRule
 
def instance = Jenkins.getInstance()

//TODO: Use docker secrets after upgrading to swarm 
// def user = new File("/run/secrets/jenkins-user").text.trim()
// def pass = new File("/run/secrets/jenkins-pass").text.trim()
 
// def hudsonRealm = new HudsonPrivateSecurityRealm(false)
// hudsonRealm.createAccount(user, pass)
// instance.setSecurityRealm(hudsonRealm)

//TODO: Run the following to create the secrets:

// echo "admin" | docker secret create jenkins-user -
// echo "admin" | docker secret create jenkins-pass -

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("admin", "admin")
instance.setSecurityRealm(hudsonRealm)
 
 
def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()
 
Jenkins.instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)
