// Copied from https://github.com/halkeye/docker-jenkins/blob/master/groovy/0003-disableRemoting.groovy
import jenkins.model.Jenkins

jenkins.model.Jenkins.instance.getDescriptor("jenkins.CLI").get().setEnabled(false)