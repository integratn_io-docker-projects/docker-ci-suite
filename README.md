# Docker based Continuous Integration Containers

## Pre-Requisites

- Docker (for Mac OSX / Windows / Docker)
- Docker Compose
- Clone the repository

## QuickStart

- To Start containers in the background

      	$ docker-compose up -d

* Stoping all containers

      	$ docker-compose down

## The result of the compose is multi-fold

- Build Jenkins with Java 8, Maven 3, Gitlab, Nexus, Postgres, SonarQube (Latest) and Nginx Images
- Start the respective containers with port mapping
- Configure SonarQube with Postgres
- Mount docker volumes for persisting the changes done at container level
- Configure nginx reverse-proxy with urls relative to localhost

- Jenkins: http://localhost/jenkins/
- Nexus: http://localhost/nexus/
- Sonar: http://localhost/sonar/
- Gitlab: http://localhost/gitlab/

## "Unlock Jenkins" - Initial Access

Shell access into Jenkins Container

`$docker exec -it jenkins /bin/bash`

`$cat /var/jenkins_home/secrets/initialAdminPassword`

Copy and Paste the Value into Unlock Screen in Jenkins Web Console

## To Start Individual Application Containers

## Jenkins

docker run --name jenkins -p 10001:8080 -p 50000:50000 -d jenkins-jdk8 --prefix=/jenkins/

## Nexus

docker run -d --name nexus-db sonatype/nexus /bin/true
docker run -e "CONTEXT_PATH=/nexus" -d -p 10002:8081 --name nexus --volumes-from nexus-db sonatype/nexus

## Sonarqube (with Postgres DB):

docker run -d --name postgres-volume postgres /bin/true
docker run -d -e "SONARQUBE_WEB_CONTEXT=/sonar" --name sonarqube -p 10003:9000 -p 9092:9092 sonarqube "-Dsonar.web.context=/sonar"
docker run -d --name postgres --volumes-from postgres-volume -e "POSTGRES_USER=sonar" -e "POSTGRES_PASSWORD=sonar" --net container:sonarqube postgres

## Nginx based Proxy:

docker run --name proxy --link jenkins --link nexus --link sonarqube -d -p 8080:80 -d proxy

## Deploy as swarm

Place Docker into swarm mode:

`$ docker swarm init`

Create a registry service for the swarm:

`$ docker service create --name registry --publish published=5000,target=5000 registry:2`

Check that the registry is running:

`$ docker service ls`

Test the app with docker-compose. This will also push the images to your new registry:

`$ docker-compose up -d`

Bring it back down:

`$ docker-compose down --volumes`

Push your generated images to the repository:

`$docker-compose push`

Deploy the stack to the swarm:

`$ docker stack deploy --compose-file docker-compose.yml ci-suite`

Check that it is running:

`$ docker stack services ci-suite`

You can bring it down when you are done with:

`$ docker service rm ci-suite`

## TODO

remove passwords from configuration files.
